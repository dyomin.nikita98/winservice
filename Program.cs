﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Management;
using System.Threading;
using System.Management.Instrumentation;
using OpenHardwareMonitor.Hardware;
using System.Net.NetworkInformation;
using MySql.Data.MySqlClient;

namespace testingMAG
{
    class Program
    {
        static void Main(string[] args)
        {
            test();
            Timer t = new Timer(Running.TimerCallback, null, 0, 1000);
            Console.ReadKey(true);
        }
        public static void test()
        {
            string name = "testing";

            DB db = new DB();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command = new MySqlCommand("SELECT * FROM `test`", db.getConnection());

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                Console.WriteLine("2");
            }
            else
            {
                Console.WriteLine("ERROR");
            }
        }
    }
    class Running
    {
        
        static Memory memory = new Memory();
        static CPU cpu = new CPU();
        static GPU gpu = new GPU();
        static RAM ram = new RAM();
        static MAC mac = new MAC();
        
        static string aboutCPU;
        static string aboutGPU;
        static string aboutRAM;
        static string aboutMemory;
        static string macAdress;

        static string powerON = "Компьютер включился в : " + DateTime.Now.ToString("dd MMMM yyyy | HH:mm:ss") + "\n";

        public static void TimerCallback(object o)
        {
            macAdress = mac.GetMAC() + "\n";
            aboutCPU = cpu.getInfo() + cpu.getLoad() + cpu.getTemp() + "\n";
            aboutGPU = gpu.getInfo() + gpu.getLoad()+ gpu.getTemperature() + "\n";
            aboutRAM = ram.getInfo() + ram.getLoad() + "\n";
            aboutMemory =  memory.getInfo() + "\n";
            File.WriteAllText("test.txt",  macAdress + powerON + aboutCPU + aboutGPU + aboutRAM + aboutMemory);
        }
             
    }
    class MAC 
    {
        //Получение MAC адреса
        public string GetMAC()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = "MAC adress: " + adapter.GetPhysicalAddress().ToString() + "\n";
                }
            }
            return sMacAddress;
        }
    }
    class GPU
    {
        string info;
        string temperature;
        string load;
        Computer c = new Computer() {
            GPUEnabled = true 
        };
        
        //Получение общей информации о видеокарте
        public string getInfo()
        {
            ManagementObjectSearcher searcherGPU = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_VideoController");

            foreach (ManagementObject queryObj in searcherGPU.Get())
            {
                info = null;
                info += "------------- Win32_VideoController instance ---------------" + "\n";
                info += " Caption: " + queryObj["Caption"] + "\n";
            }
            info += "\n";
            return info;
        }

        //Получение нагрузки видеокарты

        public string getLoad()
        {
            c.Open();
            foreach (var hardware in c.Hardware)
            {
                if (hardware.HardwareType == HardwareType.GpuNvidia)
                {
                    hardware.Update();
                    foreach (var sensor in hardware.Sensors)
                    {
                        if (sensor.SensorType == SensorType.Load)
                        {
                            load = "Нагрузка на видеокарту: " + (sensor.Value.GetValueOrDefault().ToString("0.00")) +"%"+ "\n";
                        }
                    }
                }
            }
            return load;
        }

        //Получение температуры видеокарты
        public string getTemperature()
        {
            c.Open();
            foreach (var hardware in c.Hardware)
            {
                if (hardware.HardwareType == HardwareType.GpuNvidia)
                {
                    hardware.Update();
                    foreach (var sensor in hardware.Sensors)
                    {
                        if (sensor.SensorType == SensorType.Temperature)
                        {
                            temperature = "Температура видеокарты: " + sensor.Value.GetValueOrDefault() + "°" + "\n";
                        }
                    }
                }
            }
            return temperature;
        }

    }

    class CPU
    {
        string info;
        string temperature;
        string load;
        Computer c = new Computer()
        {
            CPUEnabled = true
        };
        //Получение общей информации о процессоре
        public string getInfo()
        {

            ManagementObjectSearcher searcherCPU = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Processor");

            foreach (ManagementObject queryObj in searcherCPU.Get())
            {
                info = null;
                info += ("------------- Win32_Processor instance ---------------") + "\n";
                info += " Name: " + queryObj["Name"] + "\n";
                info += " NumberOfCores: " + queryObj["NumberOfCores"] + "\n";
                info += " ProcessorId: " + queryObj["ProcessorId"] + "\n";
            }
            info += "\n";
            return info;
        }
        //Получение загрузки процессора
        public string getLoad() {
            c.Open();
            foreach (var hardware in c.Hardware) {
                if (hardware.HardwareType == HardwareType.CPU)
                {
                    hardware.Update();
                    foreach (var sensors in hardware.Sensors) {
                        if (sensors.SensorType == SensorType.Load)
                        {
                            load = "Нагрузка на процессор: " + (sensors.Value.GetValueOrDefault().ToString("0.00")) + "%" + "\n";
                        }
                        //load=sensors.Name + ": " + sensors.Value.GetValueOrDefault();
                    }
                }
            }
            return load;
        }
        //Получение температуры процессора
        public string getTemp() {
            c.Open();
            foreach (var hardware in c.Hardware)
            {
                if (hardware.HardwareType == HardwareType.CPU)
                {
                    hardware.Update();
                    foreach (var sensors in hardware.Sensors)
                    {
                        if (sensors.SensorType == SensorType.Temperature)
                            temperature = "Температура процессора: " + sensors.Value.GetValueOrDefault() + "°" + "\n";
                    }
                }
            }
            return temperature;
        }             
}
    class RAM
    {
        string info;
        double usedMemory;
        string usedPercent;
        double totalRam;
        string totalRamString;
        Computer c = new Computer()
        {
            CPUEnabled = true
        };
        //Получение общей информации о оперативной памяти
        public string getInfo()
        {
            info = null;
            ManagementObjectSearcher searcherRAM = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PhysicalMemory");

            info += ("------------- Win32_PhysicalMemory instance --------") + "\n";
            foreach (ManagementObject queryObj in searcherRAM.Get())
            {
                info += " BankLabel: " + queryObj["BankLabel"] + "\n";
                info += " Capacity: " + Math.Round(System.Convert.ToDouble(queryObj["Capacity"]) / 1024 / 1024 / 1024, 2) + " ГБ " + "\n";
                info += " Speed: " + queryObj["Speed"] + "\n";

            }
            info += "\n";
            return info;
        }

        //Получение загрузки оперативной памяти
        public string getLoad()
        {
            ManagementObjectSearcher ramMonitor =    //запрос к WMI для получения памяти ПК
            new ManagementObjectSearcher("SELECT TotalVisibleMemorySize,FreePhysicalMemory FROM Win32_OperatingSystem");

            foreach (ManagementObject objram in ramMonitor.Get())
            {
                totalRam = Convert.ToUInt64(objram["TotalVisibleMemorySize"]);
                totalRamString = "Общий объем оперативной памяти: " + ((Convert.ToUInt64(objram["TotalVisibleMemorySize"])/1048)) + " MB" +"\n";    //общая память ОЗУ в формате String
                usedMemory = totalRam - Convert.ToUInt64(objram["FreePhysicalMemory"]);         //занятная память = (total-free)
                usedPercent = "Оперативная память загружена на: " + (((usedMemory * 100) / totalRam).ToString("0.00")) + "%" + "\n";       //вычисляем проценты занятой памяти
            }

            return totalRamString + usedPercent;
        }

    }
    class Memory
    {
        string info;
        //Получение общей информации о жестких дисках и памяти
        public string getInfo()
        {
            info = null;
            foreach (var drive in DriveInfo.GetDrives())
            {
                try
                {

                    info += " Имя диска: " + drive.Name + "\n";
                    info += " Файловая система: " + drive.DriveFormat + "\n";
                    info += " Тип диска: " + drive.DriveType + "\n";
                    info += " Объем доступного свободного места (в гигабайтах): " + drive.AvailableFreeSpace / 1024 / 1024 / 1024 + " ГБ " + "\n";
                    info += " Общий объем свободного места, доступного на диске (в гигабайтах): " + drive.TotalFreeSpace / 1024 / 1024 / 1024 + " ГБ " + "\n";
                    info += " Размер диска (в гигабайтах): " + drive.TotalSize / 1024 / 1024 / 1024 + " ГБ " + "\n";
                    info += " Метка тома диска: " + drive.VolumeLabel + "\n";
                }
                catch { }

                info += "\n";

            }
            return info;
        }
    }
}
